# microservices-ci

Build Dockerimage to initialize Jenkins:
`docker image build -t jenkins-docker .`

Or use existing one:
`docker pull glko/jenkins:v1`

Run image:
`docker container run -d -p 8080:8080 -v /var/run/docker.sock:/var/run/docker.sock jenkins-docker`